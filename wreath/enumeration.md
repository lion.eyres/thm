First 15k port scan 
		┌──(robot㉿pen-host)-[~]
		└─$ nmap -p0-15000 10.200.57.200
		Starting Nmap 7.92 ( https://nmap.org ) at 2022-04-17 14:06 EEST
		Nmap scan report for 10.200.57.200
		Host is up (0.044s latency).
		Not shown: 14954 filtered tcp ports (no-response), 42 filtered tcp ports (host-unreach)
		PORT      STATE  SERVICE
		22/tcp    open   ssh
		80/tcp    open   http
		443/tcp   open   https
		9090/tcp  closed zeus-admin
		10000/tcp open   snet-sensor-mgmt
		Nmap done: 1 IP address (1 host up) scanned in 37.57 seconds

Nmap with OS detection: 
		┌──(robot㉿pen-host)-[~]
		└─$ sudo nmap -p0-15000 -O -v 10.200.57.200
		Starting Nmap 7.92 ( https://nmap.org ) at 2022-04-18 21:33 EEST
		Initiating Ping Scan at 21:33
		Scanning 10.200.57.200 [4 ports]
		Completed Ping Scan at 21:33, 0.07s elapsed (1 total hosts)
		Initiating Parallel DNS resolution of 1 host. at 21:33
		Completed Parallel DNS resolution of 1 host. at 21:33, 0.00s elapsed
		Initiating SYN Stealth Scan at 21:33
		Scanning 10.200.57.200 [15001 ports]
		Discovered open port 80/tcp on 10.200.57.200
		Discovered open port 22/tcp on 10.200.57.200
		Discovered open port 443/tcp on 10.200.57.200
		Discovered open port 10000/tcp on 10.200.57.200
		Completed SYN Stealth Scan at 21:34, 41.79s elapsed (15001 total ports)
		Initiating OS detection (try #1) against 10.200.57.200
		Retrying OS detection (try #2) against 10.200.57.200
		Nmap scan report for 10.200.57.200
		Host is up (0.045s latency).
		Not shown: 14947 filtered tcp ports (no-response), 47 filtered tcp ports (admin-prohibited)
		PORT      STATE  SERVICE
		22/tcp    open   ssh
		80/tcp    open   http
		443/tcp   open   https
		7781/tcp  closed accu-lmgr
		7782/tcp  closed unknown
		9090/tcp  closed zeus-admin
		10000/tcp open   snet-sensor-mgmt
		Aggressive OS guesses: HP P2000 G3 NAS device (91%), Linux 2.6.32 (90%), Linux 2.6.32 - 3.1 (90%), Linux 5.0 (90%), Linux 5.1 (90%), Ubiquiti AirOS 5.5.9 (90%), Linux 5.0 - 5.4 (89%), Ubiquiti Pico Station WAP (AirOS 5.2.6) (89%), Linux 2.6.32 - 3.13 (89%), Linux 3.0 - 3.2 (89%)
		No exact OS matches for host (test conditions non-ideal).
		Uptime guess: 31.846 days (since Fri Mar 18 00:16:15 2022)
		TCP Sequence Prediction: Difficulty=263 (Good luck!)
		IP ID Sequence Generation: All zeros

		Read data files from: /usr/bin/../share/nmap
		OS detection performed. Please report any incorrect results at https://nmap.org/submit/ .
		Nmap done: 1 IP address (1 host up) scanned in 45.70 seconds
				   Raw packets sent: 30047 (1.325MB) | Rcvd: 83 (6.840KB)


Nmap with http-headers 
		┌──(robot㉿pen-host)-[~]
		└─$ sudo nmap -p0-15000 -sV --script=http-headers  10.200.57.200
		Starting Nmap 7.92 ( https://nmap.org ) at 2022-04-18 21:41 EEST
		Nmap scan report for 10.200.57.200
		Host is up (0.044s latency).
		Not shown: 14950 filtered tcp ports (no-response), 44 filtered tcp ports (admin-prohibited)
		PORT      STATE  SERVICE    VERSION
		22/tcp    open   ssh        OpenSSH 8.0 (protocol 2.0)
		80/tcp    open   http       Apache httpd 2.4.37 ((centos) OpenSSL/1.1.1c)
		|_http-server-header: Apache/2.4.37 (centos) OpenSSL/1.1.1c
		| http-headers: 
		|   Date: Mon, 18 Apr 2022 18:41:53 GMT
		|   Server: Apache/2.4.37 (centos) OpenSSL/1.1.1c
		|   Location: https://thomaswreath.thm
		|   Content-Length: 208
		|   Connection: close
		|   Content-Type: text/html; charset=iso-8859-1
		|   
		|_  (Request type: GET)
		443/tcp   open   ssl/http   Apache httpd 2.4.37 ((centos) OpenSSL/1.1.1c)
		|_http-server-header: Apache/2.4.37 (centos) OpenSSL/1.1.1c
		| http-headers: 
		|   Date: Mon, 18 Apr 2022 18:41:52 GMT
		|   Server: Apache/2.4.37 (centos) OpenSSL/1.1.1c
		|   Last-Modified: Sat, 07 Nov 2020 22:15:05 GMT
		|   ETag: "3c17-5b38ba949f993"
		|   Accept-Ranges: bytes
		|   Content-Length: 15383
		|   Connection: close
		|   Content-Type: text/html; charset=UTF-8
		|   
		|_  (Request type: HEAD)
		7781/tcp  closed accu-lmgr
		7782/tcp  closed unknown
		9090/tcp  closed zeus-admin
		10000/tcp open   http       MiniServ 1.890 (Webmin httpd)
		| http-headers: 
		|   Server: MiniServ/1.890
		|   Date: Mon, 18 Apr 2022 18:41:53 GMT
		|   Content-type: text/html; Charset=iso-8859-1
		|   Connection: close
		|   
		|_  (Request type: GET)

		Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
		Nmap done: 1 IP address (1 host up) scanned in 82.21 seconds

Look for any vulnerability on the existing services. A quick goole seach reveals that “MiniServ 1.890 (Webmin httpd)” is vulnerable for an authenticated remote code execution that is reported on CVE-2019-15107 (https://www.infosecmatter.com/nessus-plugin-library/?id=127911) 